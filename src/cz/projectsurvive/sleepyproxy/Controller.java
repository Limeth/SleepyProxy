package cz.projectsurvive.sleepyproxy;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import java.time.LocalTime;

public class Controller implements EventHandler<ActionEvent>
{
	@FXML private TextField portField;
	@FXML private ComboBox<Integer> fromHour;
	@FXML private ComboBox<Integer> fromMinute;
	@FXML private ComboBox<Integer> toHour;
	@FXML private ComboBox<Integer> toMinute;
	@FXML private Button launchButton;

	public void initialize()
	{
		prepareComboBox(fromHour, 24, 21);
		prepareComboBox(fromMinute, 60, 30);
		prepareComboBox(toHour, 24, 3);
		prepareComboBox(toMinute, 60, 0);

		launchButton.addEventHandler(ActionEvent.ACTION, this);
	}

	private void prepareComboBox(ComboBox<Integer> comboBox, int amount, int defaultValue)
	{
		for(int i = 0; i < amount; i++)
			comboBox.getItems().add(i);

		comboBox.setValue(defaultValue);
	}

	@Override
	public void handle(ActionEvent event)
	{
		try
		{
			handleUnsafe(event);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	private void handleUnsafe(ActionEvent event)
	{
		int port = Integer.parseInt(portField.getCharacters().toString());
		LocalTime from = LocalTime.of(fromHour.getValue(), fromMinute.getValue());
		LocalTime to = LocalTime.of(toHour.getValue(), toMinute.getValue());

		SleepyProxyManager.getInstance().setInterval(from, to);
		SleepyProxyManager.getInstance().startProxy(port);
		SleepyProxy.primaryStage.close();
	}
}
