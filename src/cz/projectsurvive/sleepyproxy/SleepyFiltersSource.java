package cz.projectsurvive.sleepyproxy;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;
import org.littleshoot.proxy.HttpFilters;
import org.littleshoot.proxy.HttpFiltersAdapter;
import org.littleshoot.proxy.HttpFiltersSourceAdapter;

/**
 * @author Limeth
 */
public class SleepyFiltersSource extends HttpFiltersSourceAdapter
{
	@Override
	public HttpFilters filterRequest(HttpRequest originalRequest, ChannelHandlerContext ctx)
	{
		return new HttpFiltersAdapter(originalRequest, ctx)
		{
			@Override
			public HttpResponse requestPre(HttpObject httpObject)
			{
				if(SleepyProxyManager.getInstance().isSleeping())
				{
					System.out.println("Blocked.");

					return new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.FORBIDDEN);
				}

				return super.requestPost(httpObject);
			}
		};
	}
}
