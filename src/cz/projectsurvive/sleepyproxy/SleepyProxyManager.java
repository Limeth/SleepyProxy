package cz.projectsurvive.sleepyproxy;

import org.littleshoot.proxy.HttpProxyServer;
import org.littleshoot.proxy.HttpProxyServerBootstrap;
import org.littleshoot.proxy.impl.DefaultHttpProxyServer;

import java.time.LocalTime;

/**
 * @author Limeth
 */
public class SleepyProxyManager
{
	private static final SleepyProxyManager instance = new SleepyProxyManager();
	private HttpProxyServer server;
	private LocalTime from, to;

	private SleepyProxyManager()
	{
	}

	public boolean isSleeping()
	{
		LocalTime now = LocalTime.now();

		if(now.isAfter(from))
			return !to.isAfter(from) || to.isAfter(now);
		else
			return !to.isAfter(from) && to.isAfter(now);
	}

	public void startProxy(int port)
	{
		try
		{
			HttpProxyServerBootstrap serverBootstrap = DefaultHttpProxyServer.bootstrap();

			serverBootstrap.withPort(port);
			serverBootstrap.withListenOnAllAddresses(true);
			serverBootstrap.withFiltersSource(new SleepyFiltersSource());

			server = serverBootstrap.start();

			System.out.println("Started on: " + port);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public void stopProxy()
	{
		server.stop();
		server = null;
	}

	public boolean isProxyRunning()
	{
		return server != null;
	}

	public static SleepyProxyManager getInstance()
	{
		return instance;
	}

	public void setInterval(LocalTime from, LocalTime to)
	{
		this.from = from;
		this.to = to;
	}
}
